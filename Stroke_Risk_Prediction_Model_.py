#!/usr/bin/env python
# coding: utf-8

# # Stroke Risk Prediction(Model)
# #By- Aarush Kumar
# #Dated: June 09,2021

# In[1]:


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# In[2]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Stroke Risk Prediction /stroke.csv')


# In[3]:


df


# In[5]:


df.shape


# In[6]:


df.size


# In[7]:


df.info()


# In[8]:


df.isnull().sum()


# In[9]:


df['bmi'].value_counts()


# In[10]:


df['bmi'].describe()


# In[11]:


df['bmi'].fillna(df['bmi'].mean(),inplace=True)


# In[12]:


df['bmi'].describe()


# In[13]:


df.isnull().sum()


# In[14]:


df.drop('id',axis=1,inplace=True)


# In[15]:


df


# In[16]:


from matplotlib.pyplot import figure
figure(num=None, figsize=(8, 6), dpi=800, facecolor='w', edgecolor='k')


# In[17]:


df.plot(kind='box')
plt.show()


# In[18]:


df.head()


# In[19]:


from sklearn.preprocessing import LabelEncoder
enc=LabelEncoder()


# In[20]:


gender=enc.fit_transform(df['gender'])


# In[21]:


smoking_status=enc.fit_transform(df['smoking_status'])


# In[22]:


work_type=enc.fit_transform(df['work_type'])
Residence_type=enc.fit_transform(df['Residence_type'])
ever_married=enc.fit_transform(df['ever_married'])


# In[23]:


df['work_type']=work_type


# In[24]:


df['ever_married']=ever_married
df['Residence_type']=Residence_type
df['smoking_status']=smoking_status
df['gender']=gender


# In[25]:


df


# In[26]:


df.info()


# In[27]:


X=df.drop('stroke',axis=1)


# In[28]:


X.head()


# In[29]:


Y=df['stroke']


# In[30]:


Y


# In[31]:


from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test=train_test_split(X,Y,test_size=0.2,random_state=10)


# In[32]:


X_train


# In[33]:


Y_train


# In[34]:


X_test


# In[35]:


Y_test


# In[36]:


df.describe()


# In[37]:


from sklearn.preprocessing import StandardScaler
std=StandardScaler()


# In[38]:


X_train_std=std.fit_transform(X_train)
X_test_std=std.transform(X_test)


# In[39]:


import pickle
import os


# In[40]:


scaler_path=os.path.join('/home/aarush100616/Downloads/Projects/Stroke Risk Prediction /scaler.pkl')
with open(scaler_path,'wb') as scaler_file:
    pickle.dump(std,scaler_file)


# In[41]:


X_train_std


# In[42]:


X_test_std


# In[43]:


from sklearn.tree import DecisionTreeClassifier
dt=DecisionTreeClassifier()


# In[44]:


dt.fit(X_train_std,Y_train)


# In[45]:


dt.feature_importances_


# In[46]:


X_train.columns


# In[47]:


Y_pred=dt.predict(X_test_std)


# In[48]:


Y_pred


# In[49]:


from sklearn.metrics import accuracy_score


# In[50]:


ac_dt=accuracy_score(Y_test,Y_pred)


# In[51]:


ac_dt


# In[52]:


import joblib
model_path=os.path.join('/home/aarush100616/Downloads/Projects/Stroke Risk Prediction /dt.sav')
joblib.dump(dt,model_path)


# In[53]:


from sklearn.linear_model import LogisticRegression
lr=LogisticRegression()


# In[54]:


lr.fit(X_train_std,Y_train)


# In[55]:


Y_pred_lr=lr.predict(X_test_std)


# In[56]:


Y_pred_lr


# In[57]:


ac_lr=accuracy_score(Y_test,Y_pred_lr)


# In[58]:


ac_lr


# In[59]:


from sklearn.neighbors import KNeighborsClassifier
knn=KNeighborsClassifier()


# In[60]:


knn.fit(X_train_std,Y_train)


# In[61]:


Y_pred=knn.predict(X_test_std)


# In[62]:


ac_knn=accuracy_score(Y_test,Y_pred)


# In[63]:


ac_knn


# In[64]:


from sklearn.ensemble import RandomForestClassifier
rf=RandomForestClassifier()


# In[65]:


rf.fit(X_train_std,Y_train)


# In[66]:


Y_pred=rf.predict(X_test_std)


# In[67]:


ac_rf=accuracy_score(Y_test,Y_pred)


# In[68]:


ac_rf


# In[69]:


from sklearn.svm import SVC


# In[70]:


sv=SVC()


# In[71]:


sv.fit(X_train_std,Y_train)


# In[72]:


Y_pred=sv.predict(X_test_std)


# In[73]:


ac_sv=accuracy_score(Y_test,Y_pred)


# In[74]:


ac_sv


# In[75]:


plt.bar(['Decision Tree','Logistic','KNN','Random Forest','SVM'],[ac_dt,ac_lr,ac_knn,ac_rf,ac_sv])
plt.xlabel("Algorithms")
plt.ylabel("Accuracy")
plt.show()

